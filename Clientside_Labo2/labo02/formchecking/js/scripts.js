/*
 * Main scripts
 *
 * @author Rogier van der Linde <rogier@bitmatters.be>
 */

;(function() {
    'use strict';

    // wait till DOM is loaded
    window.addEventListener('load', function() {
        var checkboxOther  = document.getElementById('qstInterests_other');
        var textboxOther = document.getElementById('qstInterests_inp_other');

        checkboxOther.addEventListener('click', function() {
            if(checkboxOther.checked == true) {
                textboxOther.style.display = 'block';
            } else {
                textboxOther.style.display = 'none';
            }
        });


        // add novalidate to form
        document.getElementById('form1').setAttribute('novalidate', 'novalidate');

        // intercept document submit
        document.getElementById('form1').addEventListener('submit', function(e) {
            // halt event
            e.preventDefault();
            e.stopPropagation();


            // form checking
            var isValid = true;

            // regular expressions check
            var ck_name = /^[A-Za-z ]{3,20}$/;
            var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

            // error messages shortcuts
            var errName = document.getElementById('errName');
            var errEmail = document.getElementById('errEmail');
            var errStreet = document.getElementById('errStreet');
            var errZip = document.getElementById('errZip');
            var errCity = document.getElementById('errCity');
            var errCountry = document.getElementById('errCountry');
            var errInterests = document.getElementById('errInterests');
            var errShare = document.getElementById('errShare');
            var errAccept = document.getElementById('errAccept');


            // input shortcuts
            var qstName = document.getElementById('qstName');
            var qstEmail = document.getElementById('qstEmail');
            var qstStreet = document.getElementById('qstStreet');
            var qstZip = document.getElementById('qstZip');
            var qstCity = document.getElementById('qstCity');
            var qstCountry = document.getElementById('qstCountry');
            var qstAccept = document.getElementById('qstAccept');


            //radiobutton shortcuts
            var btnYes = document.getElementById('share_yes');
            var btnNo = document.getElementById('share_no');


            // hide all error messages
            var errMessages = document.querySelectorAll('.message--error');
            for (var i = 0; i < errMessages.length; i++) {
                errMessages[i].style.display = 'none';
            }


            // check name
            if (!ck_name.test(qstName.value)) {
                isValid = false;
                errName.innerHTML = 'Gelieve een naam in te vullen';
                errName.style.display = 'block';
                qstName.classList.remove('valid');
                qstName.classList.add('invalid');
            } else {
                qstName.classList.add('valid');
            }


            // check email
            if (!ck_email.test(qstEmail.value)) {
                isValid = false;
                errEmail.innerHTML = 'Gelieve een emailadres in te vullen'
                errEmail.style.display = 'block';
                qstEmail.classList.remove('valid');
                qstEmail.classList.add('invalid');
            } else {
                qstEmail.classList.add('valid');
            }


            // check street and number
            if (qstStreet.value == '') {
                isValid = false;
                errStreet.innerHTML = 'gelieve een straat en nummer in te vullen';
                errStreet.style.display = 'block';
                qstStreet.classList.add('invalid');
            } else {
                qstStreet.classList.add('valid');
            }


            // check zip
            if (qstZip.value == '') {
                isValid = false;
                errZip.innerHTML = 'gelieve een postcode in te vullen';
                errZip.style.display = 'block';
                qstZip.classList.add('invalid');
            } else {
                qstZip.classList.add('valid');
            }


            // check city
            if (qstCity.value == '') {
                isValid = false;
                errCity.innerHTML = 'gelieve een gemeente in te vullen';
                errCity.style.display = 'block';
                qstCity.classList.add('invalid');
            } else {
                qstCity.classList.add('valid');
            }


            // check country
            if (qstCountry.value == '-1') {
                isValid = false;
                errCountry.innerHTML = 'Gelieve een land te kiezen';
                errCountry.style.display = 'block';
                qstCountry.classList.add('invalid');
            } else {
                qstCountry.classList.add('valid');
            }


            if (btnYes.checked == false && btnNo.checked == false) {
                isValid = false;
                errShare.innerHTML = 'gelieve een keuze te maken';
                errShare.style.display = 'block';
            };

			// check terms and agreements
			if (qstAccept.checked == false) {
				isValid = false;
				errAccept.innerHTML = 'Gelieve de gebruiksvoorwaarden te accepteren';
				errAccept.style.display = 'block';
                qstAccept.classList.add('invalid');
            } else {
                qstAccept.classList.add('valid');
            }


			// function to check the number of checkboxes that are checked
			function checkCheckboxes() {
            	var counter = 0;
				var numberOfCheckboxes = document.querySelectorAll('input[type="checkbox"]');

            	for(var i = 0; i < numberOfCheckboxes.length; i++) {
            		if(numberOfCheckboxes[i].checked == true) {
            			counter++;
					}
				}

            	return counter;
			}


            // Check checkboxes
            if (checkCheckboxes() < 2) {
                isValid = false;
                errInterests.innerHTML = 'Gelieve 2 of meer opties te kiezen';
                errInterests.style.display = 'block';
            } else if (checkCheckboxes() >= 2 && checkboxOther.checked == true) {
				if (textboxOther.value == '') {
					isValid = false;
					errInterests.innerHTML = 'Gelieve een waarde in te vullen in het daar voorziene tekstvak';
					errInterests.style.display = 'block';
				}
			}


            // draw conclusion
            if (isValid) {
                // show thank you
                document.getElementById('form1').style.display = 'none';
                document.getElementById('thankyou').style.display = 'block';
                document.getElementById('form1').onsubmit();
            } else {
                // show summary
                window.scrollTo(0,0);
                document.getElementById('summary').style.display = 'block';
            }
        });
    })
})();	
