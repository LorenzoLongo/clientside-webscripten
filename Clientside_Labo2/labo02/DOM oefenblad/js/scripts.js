/**
 * Clientside Scripting - Labo 02 - DOM
 * @author Rogier van der Linde <rogier.vanderlinde@odisee.be>
 *
 **/

;(function() { 
	'use strict';

	// wait till DOM is loaded
	window.addEventListener('load', function() {
		// reeks 1
		document.getElementById('buttonA').addEventListener('click', function() {
			console.log(document.getElementById('textfield1').value);
			var text = document.getElementById('textfield1').value;
			alert(text.toString());
		});
		document.getElementById('buttonB').addEventListener('click', function() {
			document.getElementById('textfield1').value = 'hallo';
			var text = document.getElementById('textfield1').value;
			alert(text.toString());
		});
		document.getElementById('buttonE').addEventListener('click', function() {
			document.getElementById('button1').disabled = true;
		});
		document.getElementById('buttonF').addEventListener('click', function() {
			document.getElementById('button1').disabled = false;
			document.getElementById('button1').click();
		});
		document.getElementById('buttonG').addEventListener('click', function() {
			document.getElementById('checkbox1').checked = true;
		});
		document.getElementById('buttonH').addEventListener('click', function() {
			if (document.getElementById('checkbox2').checked == false) {
				document.getElementById('checkbox2').checked = true;
			}
			else {
				document.getElementById('checkbox2').checked = false;
			}
		});
		document.getElementById('buttonI').addEventListener('click', function() {
			document.getElementById('select1').selectedIndex = 2;
		});
		document.getElementById('buttonJ').addEventListener('click', function() {
			var val = document.getElementById('select1').value;
			alert(val.toString());
		});

		// reeks 2
		document.getElementById('buttonK').addEventListener('click', function() {
			document.getElementById('cursus1').alt = 'cursus rietdekken';
		});
		document.getElementById('buttonL').addEventListener('click', function() {
			document.getElementById('cursus2').src = 'img/cursus1.jpg';
		});
		document.getElementById('buttonM').addEventListener('click', function() {
			var image = document.getElementById('cursus2');
			image.style.width = '160px';
		});

		// reeks 3
		document.getElementById('buttonN').addEventListener('click', function() {
			document.getElementById('place1').innerHTML = 'dit is plaats1';
		});
		document.getElementById('buttonO').addEventListener('click', function() {
			var span = document.getElementById('place2');
			span.style.color = 'blue';
		});
		document.getElementById('buttonP').addEventListener('click', function() {

		});
		document.getElementById('buttonQ').addEventListener('click', function() {
			var hide = document.getElementById('layer2');
			hide.style.display = 'none';
		});
	    document.getElementById('buttonR').addEventListener('click', function() {
	    	var yellow = document.getElementById('layer1');
	    	yellow.style.zIndex = '1';
		});
		document.getElementById('buttonS').addEventListener('click', function() {
			var green = document.getElementById('layer2');
			var blue = document.getElementById('layer3');

			green.style.top = '0px';
			blue.style.top = '0px';
		});

		// reeks 4
	    document.getElementById('buttonT').addEventListener('click', function() {
	    	document.body.style.backgroundColor = 'gray';
		});
		document.getElementById('buttonU').addEventListener('click', function() {
			window.location.href = 'http://www.google.be';
		});
		document.getElementById('buttonV').addEventListener('click', function() {
			document.title = 'testpagina';
		});
		document.getElementById('buttonY').addEventListener('click', function() {
			if (confirm('Wilt u de achtergrond in blauw veranderen?')) {
				document.body.style.backgroundColor = 'blue';
			}
			else {
				alert('cancelled!');
			}
		});
		document.getElementById('buttonZ').addEventListener('click', function() {
			var input = prompt('Type a color for the background:');
			document.body.style.backgroundColor = input.toString();
		});

		// reeks 5
		document.getElementById('buttonAA').addEventListener('click', function() {
			var test = document.querySelectorAll('h2');
			for(var i = 0; i < 5; i++) {
				test[i].style.color = 'green';
			}
		});
		document.getElementById('buttonAB').addEventListener('click', function() {
			var form1 = document.querySelectorAll('form1');
			
		});

		var bool = false;
		var tekst = '';
		document.getElementById('buttonAC').addEventListener('click', function() {		
			if(bool == false) {
				tekst = prompt('Geef tekst in:');
				bool = true;
			}
			else {
				alert(tekst);
			}
		});
	});
})();
