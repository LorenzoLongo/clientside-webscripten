;(function($) {
    'use strict';

    var images = ['images/bernersennen.jpg', 'images/chow.jpg', 'images/germanshepherd.jpg', 'images/goldenretriever.jpg', 'images/husky.jpg', 'images/labrador.jpg', 'images/samoyed.jpeg', 'images/shiba.jpg'];
    var imagesIndex = [0, 0, 0, 0, 0, 0, 0, 0];

    var fronts = $('.front').toArray();
    var backs = $('.back').toArray();

    var countClicks = 0;
    var nrOfCardsLeft = 16;
    var indexFirstImage;
    var checkClickedImages = [];


    var initialize = function() {
        $('.back').each(function() {
            var bool = false;

            while(bool == false) {
                var index = Math.floor((Math.random() * 8));
                imagesIndex[index]++;

                if(imagesIndex[index] <= 2) {
                    $(this).append('<img src="' + images[index] + '" width="150px" height="150px" />');
                    bool = true;
                }
            }
        });
    };

    var checkClickedCards = function(checkClickedImages) {
        if(checkClickedImages[0] == checkClickedImages[1]) {
            return true;
        }
        return false;
    };

    var checkEndOfGame = function() {
        if(nrOfCardsLeft == 0) {
            $('.memory-container').append('<p>Gefeliciteerd! Het memoryspel is geslaagd!</p>').css('background-color', 'green').css('font-weight', '24px');
            $('.memory-container').prop('disabled', true);
        }
    };

    $(window).on('load', function() {
        console.log('loaded');

        $(fronts).on('click', function() {
            if (countClicks < 2) {
                countClicks++;

                var indexFront = $(fronts).index($(this));
                var div = $(backs).get(indexFront);
                var backImage = $(div).find('img').clone();
                $(this).append(backImage);

                if(countClicks == 1) {
                    checkClickedImages[0] = backImage.attr('src');
                    indexFirstImage = indexFront;
                }
                else if(countClicks == 2) {
                    checkClickedImages[1] = backImage.attr('src');
                    if(checkClickedCards(checkClickedImages)) {
                        $(fronts).eq(indexFront).removeClass('front');
                        $(fronts).eq(indexFirstImage).removeClass('front');
                        nrOfCardsLeft = nrOfCardsLeft - 2;
                        checkEndOfGame();
                    }
                }
            } else {
                $('.front').children('img').remove();
                countClicks = 0;
            }
        });

        initialize();
    });
})(jQuery);