;(function($) {
	'use strict'

	$(window).on('load', function() {
		console.log('loaded');

		// CSS 

		// 1
		$('#btnCss1a').on('click', function() {
			$('#txtCss1').addClass('error');		
		});

		// 2
		$('#btnCss1b').on('click', function() {
			$('#txtCss1').removeClass('error');
		});

		// 3
		$('#btnCss1c').on('click', function() {
			$('#txtCss1').css('color', 'blue');
		});

		// 4
		$('#btnCss1d').on('click', function() {
			$('#txtCss1').css('color', 'green').css('font-style', 'italic');
		});

		// 5
		$('#btnCss1e').on('click', function() {
			var paragraph = $('#txtCss1');
			console.log(paragraph.css('width'));
		});


		// 6
		$('#btnCss1f').on('click', function() {
			$('#txtCss1').css('width', '600px');
		});

		// 7
		$('#btnCss1g').on('click', function() {
			$('#txtCss1').text('blablabla');
		});



		// Selectors

		// 1
		$('#btnSel1a').on('click', function() {
			var links = $('#targetSel1 a[href]');
			console.log(links.length);
		});

		// 2
		$('#btnSel1b').on('click', function() {
			var link = $('#targetSel1 a[href]:first');
			console.log(link.text());
		});

		// 3 
		$('#btnSel1c').on('click', function() {
			$('#targetSel1 li:nth-of-type(2n+1)').css('backgroundColor', 'gray');
		});

		// 4
		$('#btnSel1d').on('click', function() {
			var links = $('#targetSel1 a[href]');

			links.each(function() {
				if($(this).hasClass('removeMe')) {
					$(this).remove();
				}
			});
		});

		// 5
		$('#btnSel1e').on('click', function() {
			$('#targetSel1 li a').eq(2).css('font-weight', 'bold');
		});

		// 6
		$('#btnSel1f').on('click', function() {
			$('#targetSel1 li a').eq(6).css('font-style', 'italic');
		});

		// 7
		$('#btnSel1g').on('click', function() {
			$('#targetSel1 li a').eq(1).attr('title', 'tip van de dag: ' + $('#targetSel1 li a').eq(1).text());
		});



		// Events

		// Oefening 1
		$('#btnEvents1a').mouseover(function() {
			console.log('oefening 3.1 ok');
		});

		// Oefening 2 Deel1 
		$('#targetEvents1 a[href]').eq(0).click(function(e) {
			e.preventDefault();
			console.log('oefening 3.2 ok');
		});

		// Oefening 2 Deel2
		$('#btnEvents2a').mouseover(function() {
			console.log('button event is triggered!');
			$('#targetEvents1 li a').eq(0).off();
		}); 

		// Oefening 3 
		// Nog niet afgewerkt!!!
		// !!!!!!!!!!!!!!!!!!!!!!
		$('#targetEvents2 li a').click(function(e) {
			e.preventDefault();
			console.log($(this).index());
		});

		// Oefening 4
		// Nog niet afgewerkt!!!
		// !!!!!!!!!!!!!!!!!!!!!!
		$('#targetEvents3').click(function() {
			console.log('focus is triggered');
			$('#this').focus();
			$('#this').val('');
		});



		// Animaties

		// Oefening 1
		$('#btnAnim0').click(function() {
			$('#txtAnim0').toggle();
		});

		// Oefening 2
		$('#btnAnim1').click(function() {
			$('#txtAnim1').animate({
				opacity: 0
			}, 'slow');
		});

		// Oefening 3
		$('#btnAnim2a').click(function() {
			$('#txtAnim2').slideUp();
		});

		$('#btnAnim2b').click(function() {
			$('#txtAnim2').slideDown();
		});

		// Oefening 4
        $('#btnAnim3').click(function() {
            $('#txtAnim3').slideToggle('slow');
        });
	});
})(jQuery);