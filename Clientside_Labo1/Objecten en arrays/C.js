WScript.StdOut.WriteLine("Objecten en arrays");
WScript.StdOut.WriteLine("Opdracht C");
WScript.StdOut.WriteLine("");


var astrid = {
    Naam: 'Astrid Galli',
    Balans: 2500,
    Vrienden: ['Bilal Azzouti', 'Crista Bracke', 'Duncan Reck']
}

var bilal = {
    Naam: 'Bilal Azzouti',
    Balans: 3000,
    Vrienden: ['An Cornelis', 'Duncan Reck']
}

var claude = {
    Naam: 'Claude Chen',
    Balans: -300,
    Vrienden: ['Erwin Smith', 'Astrid Galli', 'Francis Alys']
}

var balance = 0;
var friends = [];

for(var i = 0; i < claude.Vrienden.length; i++){
    claude.Vrienden.pop();
}

WScript.StdOut.WriteLine(claude.Vrienden[0]);