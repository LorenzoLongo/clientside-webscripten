WScript.StdOut.WriteLine("Objecten en arrays");
WScript.StdOut.WriteLine("Opdracht A");
WScript.StdOut.WriteLine("");

var astrid = {
    Naam: 'Astrid Galli',
    Balans: 2500,
    Vrienden: ['Bilal Azzouti', 'Crista Bracke', 'Duncan Reck']
}

var bilal = {
    Naam: 'Bilal Azzouti',
    Balans: 3000,
    Vrienden: ['An Cornelis', 'Duncan Reck']
}

var claude = {
    Naam: 'Claude Chen',
    Balans: -300,
    Vrienden: ['Erwin Smith', 'Astrid Galli', 'Francis Alys']
}

WScript.StdOut.WriteLine(astrid.Naam + ' ' + bilal.Balans + ' ' + claude.Vrienden[0]);