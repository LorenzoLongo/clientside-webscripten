WScript.StdOut.WriteLine("Objecten en arrays");
WScript.StdOut.WriteLine("Opdracht B");
WScript.StdOut.WriteLine("");

var astrid = {
    Naam: 'Astrid Galli',
    Balans: 2500,
    Vrienden: ['Bilal Azzouti', 'Crista Bracke', 'Duncan Reck']
}

var bilal = {
    Naam: 'Bilal Azzouti',
    Balans: 3000,
    Vrienden: ['An Cornelis', 'Duncan Reck']
}

var claude = {
    Naam: 'Claude Chen',
    Balans: -300,
    Vrienden: ['Erwin Smith', 'Astrid Galli', 'Francis Alys']
}

var createCouple = function(person1, person2) {
    var unique = [];

    for(var i = 0; i < person1.Vrienden.length ; i++) {
        var found = false;

        for(var j = 0; j < person2.Vrienden.length; j++) {
            if(person1.Vrienden[i] == person2.Naam && person1.Vrienden[i] == person2.Vrienden[j]) {
                found = true;
                break;
            }
        }
        if(found == false) {
            unique.push(person1.Vrienden[i]);
        }
    }

    var couple = {
        Naam: person1.Naam + ' & ' + person2.Naam,
        Balans: person1.Balans + person2.Balans,
        Vrienden: unique
    };

    return couple;
};

WScript.StdOut.WriteLine(createCouple(astrid, bilal).Naam + '\n' + createCouple(astrid, bilal).Balans + '\n' + createCouple(astrid, bilal).Vrienden);