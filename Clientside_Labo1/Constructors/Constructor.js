WScript.StdOut.WriteLine("Constructors");
WScript.StdOut.WriteLine("");

var Disc = function(radius) {
    this.radius = radius;

    this.getCircumference = function() {
        var circ = 2 * radius * Math.PI;
        return circ;
    };
    this.getArea = function() {
        var area = radius * radius * Math.PI;
        return area;
    };
};

var circle = new Disc(3);

WScript.StdOut.WriteLine("omtrek van straal => 3: " + circle.getCircumference());
WScript.StdOut.WriteLine("oppervlakte van straal => 3: " + circle.getArea());