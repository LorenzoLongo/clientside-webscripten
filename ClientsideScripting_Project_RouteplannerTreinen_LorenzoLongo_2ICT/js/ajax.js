/**
 * Clientside Project: Routeplanner Trains
 * Ajax requests, main page form autocompletions
 * API: irail URL: https://docs.irail.be/
 *
 * @author: Lorenzo Longo
 * @email: Lorenzo.longo@student.odisee.be
 * @date: 27/10/2018
 *
 */

$(document).ready(function($j) {
    
    // VARIABLES
    // Variable arrays
    var $stationNames = [];
    var $trainRidesForSelectedStations = [];
    var $localStorageArray = [];
    var $abbreviatedMonths = [ 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC' ];

    // Form variables
    var $liveBoardStation = '';
    var $fromStation = '';
    var $toStation = '';
    var $date = '';
    var $time = '';
    var $ORIGINAL_TIME = '';
    var $map;
    var $clickedId = 0;

    // Pages
    var $indexPage = $('#mainPage');
    var $infoPage = $('#trainInfoPage');
    var $breadcrumb = $('.breadcrumb');
    var $searchLive = $('#liveboardSearch');
    var $savedTrainRides = $('#savedTrainRides');
    var $linkFormSearchTrainRides = $('#linkFormSearchTrainRides');
    var $linkFormSearchTrainRide = $('#linkFormSearchTrainRide');
    var $formSearchRides = $('#formSearchRides');
    var $formLiveboard = $('#formLiveboard');

    // Dynamically generated html is inserted in these variables
    var $formContainer = $('#formContainer');
    var $trainRidesContainer = $('#trainRidesContainer');
    var $trainRidesList = $('#trainRidesList');
    var $containerLiveboard = $('#containerLiveBoard');
    var $containerSavedTrainRides = $('#containerSavedTrainRides');
    var $containerSaved = $('#containerSaved');

    var $detailedRideContainer = $('#detailedInfoContainer');
    var $detailTrainRideInfo = $('#detailedTrainRideInfo');

    var $searchTrainRides = $('#submitBtn');
    var $closePopUpDetailedRide = $('#closeBtn');

    // Localstorage
    var $saveToLocalStorageBtn = $('#saveToStorageBtn');



    // AUTOCOMPLETIONS
    // Form autocompletions (Station names)
    $getTrainStations();

    $('#qstFromStation').autocomplete({
        source: $stationNames
    });

    $('#qstToStation').autocomplete({
        source: $stationNames
    });

    $('#qstStation').autocomplete({
        source: $stationNames
    });


    // FUNCTIONS
    // Convert to necessairy API GET parameter value (date)
    function $convertDate(date) {
        var $day = date.substring(8, 10);
        var $month = date.substring(4, 7);
        var $year = date.substring(13, 15);

        switch($month.toUpperCase()) {
            case $abbreviatedMonths[0]:
                $month = '01';
                break;
            case $abbreviatedMonths[1]:
                $month = '02';
                break;
            case $abbreviatedMonths[2]:
                $month = '03';
                break;
            case $abbreviatedMonths[3]:
                $month = '04';
                break;
            case $abbreviatedMonths[4]:
                $month = '05';
                break;
            case $abbreviatedMonths[5]:
                $month = '06';
                break;
            case $abbreviatedMonths[6]:
                $month = '07';
                break;
            case $abbreviatedMonths[7]:
                $month = '08';
                break;
            case $abbreviatedMonths[8]:
                $month = '09';
                break;
            case $abbreviatedMonths[9]:
                $month = '10';
                break;
            case $abbreviatedMonths[10]:
                $month = '11';
                break;
            case $abbreviatedMonths[11]:
                $month = '12';
                break;
        }

        return $day + '' + $month + '' + $year;
    }

    // Convert to necessairy API GET parameter value (time)
    function $convertTime(time) {
        return time.replace(':', '');
    }

    // Convert result to Date (To use inside HTML)
    function $convertToVisualDate(dateTime) {
        return new Date(dateTime * 1e3).toISOString().slice(-13, -5);
    }


    // EVENTS
    // Breadcrumb to homepage event
    $breadcrumb.on('click', function(e) {
        $trainRidesList.empty();
        $('#trainTitle').remove();
        $trainRidesForSelectedStations.length = 0;
        $detailedRideContainer.css('display', 'none');
        $infoPage.css('display', 'none');
        $('input').removeClass('valid').removeClass('invalid');
        $('#form').trigger('reset');
        $indexPage.css('display', 'block');
    });

    // Event triggered to get all train rides for the selected stations inside the form
    $searchTrainRides.on('click', function(e) {
        $getTrainRides();
    });

    // Event triggered to show more detailed info for a selected train ride (container)
    $trainRidesList.on('click', '.anchor', function(e) {
        e.preventDefault();
        $detailTrainRideInfo.empty();

        $clickedId = this.id;
        $trainRidesForSelectedStations.forEach(function(e) {

            if(e.id == $clickedId) {
                $detailTrainRideInfo.append('<h2>Detailed info to ' + e.arrival.stationinfo.standardname + ' - ' + $convertToVisualDate(e.arrival.time).substring(0,5) + '</h2><p><div class="popupTable"><div class="popupTableBody"><div class="popupRow-1"><div class="popupCell-1">' + e.departure.platform + '</div><div class="popupCell-1"></div>' + e.arrival.platform + '</div><div class="popupRow-2"><div class="popupCell-2">' + e.departure.stationinfo.standardname + '</div><div class="popupCell-2">' + e.departure.vehicle.substring(8, e.departure.vehicle.length) + '</div><div class="popupCell-2">' + e.arrival.stationinfo.standardname + '</div></div><div class="popupRow-3"><div class="popupCell-3">' + $convertToVisualDate(e.departure.time).substring(0,5) + '</div><div class="popupCell-3">' + $convertToVisualDate(e.arrival.time).substring(0,5) + '</div></div><div class="popupRow-4"><div class="popupCell-4">Price</div><div class="popupCell-4">Single</div><div class="popupCell-4">€9</div></div><div class="popupRow-5"><div class="popupCell-5">Double</div><div class="popupCell-5">€18</div></div></div></div></p>');

                $map = L.map('map', {
                    center: [e.arrival.stationinfo.locationY, e.arrival.stationinfo.locationX],
                    zoom: 16,
                    zoomOffset: 1
                });

                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                }).addTo($map);

                setTimeout(function () {
                    $map.invalidateSize();
                }, 0);

                var marker = L.marker([e.arrival.stationinfo.locationY, e.arrival.stationinfo.locationX]).addTo($map);
                marker.bindPopup('<b>' + e.arrival.stationinfo.standardname +'</b><br>Lorem Ipsum').openPopup();
            }
        });

        $detailedRideContainer.css('display', 'block');
    });

    /* Close detailed train info pop-up */
    $closePopUpDetailedRide.on('click', function(e) {
        e.preventDefault();
        $detailedRideContainer.css('display', 'none');
        $detailTrainRideInfo.empty();
        $map.remove();
        $('#addSuccesful').remove();
    });

    /* Live search for liveboard (show container) */
    $searchLive.on('click', function (e) {
        e.preventDefault();
        $formSearchRides.css('display', 'none');
        $formLiveboard.css('display', 'block');
    });

    /* Close liveboard container and open search train container */
    $linkFormSearchTrainRides.on('click', function(e) {
        e.preventDefault();
        $containerLiveboard.empty();
        $('#qstStation').val('');
        $formLiveboard.css('display', 'none');
        $formSearchRides.css('display', 'block');
    });


    /* Saves a train ride onto the browsers localstorage */
    $saveToLocalStorageBtn.on('click', function(e) {
        e.preventDefault();
        $trainRidesForSelectedStations.forEach(function(e) {
            if ($clickedId == e.id) {
                $localStorageArray.push(e);
            }
        });
        localStorage.setItem('rides', JSON.stringify($localStorageArray));

        $detailedRideContainer.append('<span id="addSuccesful">Succesfully added!</span>');
    });

    /* Show saved train rides (localstorage) inside container */
    $savedTrainRides.on('click', function (e) {
        $containerSaved.empty();
        e.preventDefault();
        const data = JSON.parse(localStorage.getItem('rides'));

        $containerSaved.append('<div class="divTable"><div class="divTableBody"><div class="divTableRow"><div class="divTableCell"><p>FROM</p></div><div class="divTableCell"><p>DEPARTURE TIME</p></div><div class="divTableCell"><p>TO</p></div><div class="divTableCell"><p>ARRIVAL</p></div><div class="divTableCell"><p>DURATION</p></div></div></div></div>');
        data.forEach(item => {
            $containerSaved.append('<div class="divTable"><div class="divTableBody"><div class="divTableRow line"><a class="liveboardLinks" href="#buy"><div class="divTableCell">' + item.departure.stationinfo.standardname + '</div><div class="divTableCell"> ' + $convertToVisualDate(item.departure.time).substring(0,5) + '</div><div class="divTableCell"> ' + item.arrival.stationinfo.standardname + '</div><div class="divTableCell"> ' + $convertToVisualDate(item.arrival.time).substring(0,5) + ' min</div><div class="divTableCell"> ' + $convertToVisualDate(item.duration).substring(0,5) + '</div></a></div></div>');
        });

        $formSearchRides.css('display', 'none');
        $containerSavedTrainRides.css('display', 'block');
    });

    /* Close saved trainrides container, open search trains form */
    $linkFormSearchTrainRide.on('click', function (e) {
        e.preventDefault();
        $containerSavedTrainRides.css('display', 'none');
        $formSearchRides.css('display', 'block');
    });

    /* Get live list of train rides for a specified train station */
    $('#qstStation').keyup(function () {
        $getLiveBoard();
    });


    // AJAX REQUESTS
    // Get all stations from api.irail.be/stations
    function $getTrainStations() {
        $.ajax({
            url: 'https://api.irail.be/stations/?format=json&lang=en',
            type: 'GET',
            dataType: 'json',
            'user-agent': 'MyRailAPP (local school project, lorenzo.longo@student.odisee.be)',
            headers: {
                'Accept': 'application/json',
            },
            success: function(result) {
                for(var i = 0; i < result.station.length; i++) {
                    $stationNames.push({label:result.station[i].standardname, value:result.station[i].standardname});
                }
                $stationNames.sort(function(a, b) {
                    if(a.label < b.label) { return -1; }
                    if(a.label > b.label) { return 1; }
                    return 0;
                });
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    // Get all train rides from the selected stations inside the form (from api.irail.be/connections)
    function $getTrainRides() {
        $fromStation = $('#qstFromStation').val();
        $toStation = $('#qstToStation').val();
        $date = $('#calendar').val();
        $time = $('#time').val();

        $ORIGINAL_TIME = $time;
        $time = $convertTime($time);
        $date = $convertDate($date);

        $.ajax({
            url: 'https://api.irail.be/connections/?from=' + $fromStation + '&to=' + $toStation + '&date=' + $date + '&time=' + $time + '&timesel=departure&format=json&lang=en',
            type: 'GET',
            dataType: 'json',
            'user-agent': 'MyRailAPP (local school project, lorenzo.longo@student.odisee.be)',
            headers: {
                'Accept': 'application/json',
            },
            success: function(result) {
                $('#trainInfoPage').css('display', 'block');
                for(var i = 0; i < result.connection.length; i++) {
                    if($convertToVisualDate(result.connection[i].departure.time) > $ORIGINAL_TIME) {
                        $trainRidesForSelectedStations.push(result.connection[i]);
                        $trainRidesList.append('<div class="layoutTrainData"><svg class="firstLine" height="10" width="1100"><line class="lines" x1="0" y1="0" x2="550" y2="0"" /></svg><img class="trainIcon"  src="images/train_beige.png" alt="train_icon"><svg class="secondLine" height="10" width="1100"><line class="lines" x1="0" y1="0" x2="550" y2="0"" /></svg><div class="departure"><div class="otherFont">Departure time: </div>' + $convertToVisualDate(result.connection[i].departure.time).substring(0,5) + '</div><div class="duration"><div class="otherFont">Duration: </div>' + $convertToVisualDate(result.connection[i].duration).substring(0,5) + '</div><div class="arrival"><div class="otherFont">Arrival time: </div> ' + $convertToVisualDate(result.connection[i].arrival.time).substring(0,5) + '</div><div class="platform"><div class="otherFont">Platform: </div>' + result.connection[i].departure.platform + '</div><div class="platform2"><div class="otherFont">Platform:</div>' + result.connection[i].arrival.platform + '</div></p><a id="' + result.connection[i].id + '" class="anchor" href="#moreInfo"></a></div>');
                    }
                }
                $trainRidesContainer.append('<h2 id="trainTitle"> Your train ride to ' + $toStation + '</h2>' );
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    /* Get all trains for a specified train station */
    function $getLiveBoard() {
        var $dateTime = new Date();
        var $currentDate = $convertDate($dateTime.toDateString());
        var $currentTime = $convertTime($dateTime.toTimeString().substring(0,5));
        $liveBoardStation = $('#qstStation').val();

        $.ajax({
            url: 'https://api.irail.be/liveboard/?station=' + $liveBoardStation +'&date=' + $currentDate + '&time=' + $currentTime + '&lang=nl&format=json',
            type: 'GET',
            dataType: 'json',
            'user-agent': 'MyRailAPP (local school project, lorenzo.longo@student.odisee.be)',
            headers: {
                'Accept': 'application/json',
            },
            success: function(result) {
                $containerLiveboard.empty();
                $containerLiveboard.append('<div class="divTable"><div class="divTableBody"><div class="divTableRow"><div class="divTableCell"><p>STATION</p></div><div class="divTableCell"><p>TIME</p></div><div class="divTableCell"><p>PLATFORM</p></div><div class="divTableCell"><p>DELAY</p></div><div class="divTableCell"><p>VEHICLE</p></div></div></div></div>');
                for(var i = 0; i < result.departures.departure.length; i++) {
                    var $parseTime = $convertToVisualDate(result.departures.departure[i].time);
                    $containerLiveboard.append('<div class="divTable"><div class="divTableBody"><div class="divTableRow line"><a class="liveboardLinks" href="#buy"><div class="divTableCell">' + result.departures.departure[i].station + '</div><div class="divTableCell"> ' + $parseTime.substring(0, 5) + '</div><div class="divTableCell"> ' + result.departures.departure[i].platform + '</div><div class="divTableCell"> ' + result.departures.departure[i].delay + ' min</div><div class="divTableCell"> ' + result.departures.departure[i].vehicle.substring(8, result.departures.departure[i].vehicle.length) + '</div></a></div></div>');
                }
            },
            error: function(error) {
                console.log(error);
            }
        }, 3000);
    }
});