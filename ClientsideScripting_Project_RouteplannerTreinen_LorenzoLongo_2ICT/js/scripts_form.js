/*
* Clientside Project: Routeplanner Trains
* Main page form validation (jQuery script)
*
* @author: Lorenzo Longo
* @email: Lorenzo.longo@student.odisee.be
* @date: 27/10/2018
*
 */

;(function($j) {
    'use strict';

    $j(window).on('load', function() {
        console.log('loaded');

        // Date and timepicker modules
        const picker = datepicker('#calendar');

        timepicker.load({
            interval: 30
        });


        $('#form').on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();

            // variables
            var $isValid = true;

            var $errFrom = $('#errFrom');
            var $errTo = $('#errTo');
            var $errCalendar = $('#errCalendar');
            var $errTime = $('#errTime');

            var $qstFrom = $('#qstFromStation');
            var $qstTo = $('#qstToStation');
            var $qstCalendar = $('#calendar');
            var $qstTime = $('#time');

            var $search = $('#submitBtn');
            var $main = $('#mainPage');


            // validations
            if($qstFrom.val() == '') {
                $isValid = false;
                $errFrom.html('Select a station!').css('display', 'block');
                $qstFrom.removeClass('valid').addClass('invalid');
            } else {
                $qstFrom.addClass('valid');
                $errFrom.css('display', 'none');
            }

            if($qstTo.val() == '') {
                $isValid = false;
                $errTo.html('Select a station!').css('display', 'block');
                $qstTo.removeClass('valid').addClass('invalid');
            } else {
                $qstTo.addClass('valid');
                $errTo.css('display', 'none');
            }

            if($qstCalendar.val() == '') {
                $isValid = false;
                $errCalendar.html('Select a date!').css('display', 'block');
                $qstCalendar.removeClass('valid').addClass('invalid');
            } else {
                $qstCalendar.addClass('valid');
                $errCalendar.css('display', 'none');
            }

            if($qstTime.val() == '') {
                $isValid = false;
                $errTime.html('Select a time!').css('display', 'block');
                $qstTime.removeClass('valid').addClass('invalid');
            } else {
                $qstTime.addClass('valid');
                $errTime.css('display', 'none');
            }

            // Hide mainpage if form is valid
            if($isValid) {
                $main.css('display', 'none');
            }
        });
    });
})(jQuery);

