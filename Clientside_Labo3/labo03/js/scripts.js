;(function() {
	'use strict';

	var table;
	var rows;
	var cells;
	var error = true;
	var btnCheck = document.getElementById('btnCheck');

	var insertKey = function(selectedCell) {
		var cell = selectedCell.textContent;

		if (cell == 1 || cell == 0) {
			selectedCell.innerHTML = cell;
		} else {
			selectedCell.innerHTML = '';
		}
	};

	var checkBoard = function() {
		var emptyCells = 0;
		var numEmptyCells = document.getElementById('numEmptyCells');

		for(var i = 0; i < cells.length; i++) {
			if(cells[i].innerHTML === '') {
				emptyCells++;
			} 
		}

		numEmptyCells.innerHTML = emptyCells;
		return emptyCells;
	};

	var checkRows = function(arr) {
		for(var i = 0; i < arr.length ; i++) {
			console.log(arr[i].innerText);
		}
	};

	var checkIfRowsAreValid = function() {
		cleanCells();
		for(var i = 0; i < rows.length; i++) {
		    var boolValid = true;
		    var countNrOfOnes = 0;
		    var countNrOfZeros = 0;
			var rowCells = rows[i].querySelectorAll('td');

			for(var j = 0; j < rowCells.length; j++){
				if(rowCells[j].innerHTML == '') {
				    boolValid = false;
				    error = true;
				    break;
                } else {
				    if(rowCells[j].innerHTML == '1') {
				        countNrOfOnes++;
                    } else if (rowCells[j].innerHTML == '0') {
				        countNrOfZeros++;
                    }
                }
			}

			if(countNrOfOnes != countNrOfZeros) {
			    boolValid = false;
            } else {
			    var count = 0;
			    for(var m = 0; m < rowCells.length ; m++) {
			        if(rowCells[m].innerHTML == '1' || rowCells[m].innerHTML == '0') {
			            count++;
                    }
                }
			    if(count == 6) {
			        boolValid = true;
                }
            }

			if(boolValid == false) {
			    for(var k = 0; k < rowCells.length ; k++) {
			        error = true;
			        rowCells[k].className = 'invalid';
                }
            }
            else if(boolValid == true) {
                for(var l = 0; l < rowCells.length; l++) {
                    rowCells[l].className = 'valid';
                    error = false;
                }
            }
		}
	};

	var cleanCells = function() {
		for(var i = 0; i < cells.length; i++) {
			cells[i].classList.remove('invalid');
			cells[i].classList.remove('valid');
		}
	};

	var checkIfFinished = function() {
	    if(checkBoard() == 0 && error == false) {
            var endTitle = document.getElementById('endGame');
            endTitle.innerHTML = 'Proficiat! U hebt de sudoku correct ingevuld!';
            endTitle.style.color = 'green';
            endTitle.style.fontSize = 'large';

            table.style.opacity = '0.5';

            for(var i = 0; i < cells.length; i++) {
                cells[i].contentEditable = 'false';
            }
        }
    };

	window.addEventListener('load', function() {

		// variables of sudoku
		table = document.getElementById('sudoku');
		rows = document.querySelectorAll('#sudoku tr');
		cells = document.querySelectorAll('#sudoku td');

		// check nr of rows and cells
		console.log(rows.length);
		console.log(cells.length);

		checkBoard();

		// update sudoku
		table.addEventListener("keyup", function(ev) {
			insertKey(ev.target);

			checkBoard();

			// stay inside cell if backspace or delete key is pressed
			var key = ev.keyCode || ev.charCode;

			if(key == 8 || key == 46) {
				console.log('testing backspace and delete buttons press not blurring');
			} else {
				ev.target.blur();
			}
		});

		// Check the sudoku
		btnCheck.addEventListener("click", function() {
			console.log('button is clicked!')
			var arr = table.querySelectorAll('td');

			// Check the rows 1.5.1
			checkRows(arr);

			// Check if the rows are filled in correctly 1.5.2
            checkIfRowsAreValid();

            // Check if sudoku is finished without an error 1.6
            checkIfFinished();
		});
	});

})();